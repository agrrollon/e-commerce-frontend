import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

//import "react-toastify/dist/ReactToastify.min.css";

import { ToastContainer } from "react-toastify";

import { AuthContextProvider } from "./store/auth-context";

ReactDOM.render(
  <AuthContextProvider>
    <ToastContainer />
    <App />
  </AuthContextProvider>,
  document.getElementById("root")
);
