import React from "react";
import { Link } from "react-router-dom";
import "./Products.css";

function ProductItem(props) {
  return (
    <>
    <div className="card-box" key={props._id}>
    <dt>
        <img src={props.src} className="card-img-top" alt="..." />
    </dt>

      <div classNames="card-body text-dark">
        <h5 className="card-title">{props.name}</h5>
        <p className="card-text text-secondary">{props.description}</p>
        <p className="card-text text-secondary">{props.price}</p>
        <Link to={`/itemview/${props.productId}`} className="btn btn-outline-success">
          View Product
        </Link>
      </div>
    </div>
      
    </>
  );
}

export default ProductItem;
