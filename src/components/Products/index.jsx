import React, { useEffect, useState } from "react";
import ProductItem from "./ProductItem";
import "./Products.css";
import { Link } from "react-router-dom";
//import AppHelper from "../../app-helper";
// import LoadingBox from "../Loading";
// import MessageBox from "../MessageBox";

const Products = () => {
  const [productLists, setProductLists] = useState([]);
  //     const [loading, setLoading] = useState(false);
  //     const [error, setError] = useState(null);
  useEffect(() => {
    fetch(`https://blooming-basin-66732.herokuapp.com/api/items/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProductLists(data);
      });
  }, []);
  console.log(productLists)

  const prodLists = productLists.map((prodList) => {

    return (
      <ProductItem
        key={prodList._id}
        productId={prodList._id}
        src={prodList.image}
        name={prodList.name}
        description={prodList.description}
        price={prodList.price}
        label="View Product"
        
      />
    );
  });

  return (
    <>
    <div className="list-container" key={productLists._id}>
        
    </div>
     
      <div className="cards" id="products">
        <div>
       <h1>Check out these Popular Interior Designs</h1>
          <dl className="products">{prodLists}</dl>
        </div>
      </div>
    </>
  );
};

export default Products;
