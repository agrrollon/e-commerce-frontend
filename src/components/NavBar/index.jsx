import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { FaShoppingBag } from "react-icons/fa";

import AuthContext from "../../store/auth-context";
import "./NavBar.css";

const NavBar = () => {
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;
  return (
    <>
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link to="/" className="navbar-logo mr-auto">
            <FaShoppingBag className="navbar-icon" />
            FurniShop
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse navbar-container"
            id="navbarNav"
          >
            <ul className="navbar-nav ">
              <li className="nav-item ">
                <Link to="/products" className="nav-link">
                  Products
                </Link>
              </li>

              {!isLoggedIn && (
                <>
                  <li className="nav-item ">
                    <Link to="/register" className="nav-link">
                      Sign Up
                    </Link>
                  </li>

                  <li className="nav-item ">
                    <Link to="/login" className="nav-link">
                      Login
                    </Link>
                  </li>
                </>
              )}

              {isLoggedIn && (
                <>
                  <li className="nav-item ">
                    <Link to="/logout" className="nav-link">
                      Logout
                    </Link>
                  </li>
                  <li className="nav-item ">
                    <Link to="/services" className="nav-link">
                      Services
                    </Link>
                  </li>
                </>
              )}
            </ul>
          </div>
        </nav>
      </div>
    </>
  );
};

export default NavBar;
