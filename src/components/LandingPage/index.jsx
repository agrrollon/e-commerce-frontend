import React from "react";
import "./LandingPage.css";

const LandingPage = () => {
  return (
    <div>
      <div className="landing-container">
        <video src="/videos/video-2.mp4" autoPlay loop muted />
        <div className="landing-header">
          <div className="landing-text">
            <h1 >FurniShop</h1>
            <h3 >Where designs meet creativity</h3>
            <h6 >Designs inspired by the new generation with a classic touch</h6>
           {/* put svg */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
