import React from "react";
import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import ItemView from "./pages/ItemView/";
import Register from "./pages/Register";
import NavBar from "./components/NavBar";
import Login from "./pages/Login";
import CartPage from "./pages/CartPage";

function App() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
        <Route path="/itemview/:id" component={ItemView} />
        <Route path="/addtocart" component={CartPage} />
      </Switch>
    </Router>
  );
}

export default App;
