import React, { useState } from "react";
import validator from "validator";

import useInputHook from "../../hooks/input/useInputHook";
const Register = () => {
  // [value, isValid, handleChange, isValidInput];

  const [firstName, isValidFName, setFirstName, setIsValidFName] =
    useInputHook("");
  const [lastName, isValidLName, setLastName, setIsValidLName] =
    useInputHook("");
  const [emailAdress, isValidEmail, setEmailAdress, setIsValidEmail] =
    useInputHook("");
  const [mobileNum, isValidMobile, setMobileNum, setIsValidMobile] =
    useInputHook("");
  const [password1, isValidPassword1, setPassword1, setIsValidPassword1] =
    useInputHook("");
  const [password2, isValidPassword2, setPassword2, setIsValidPassword2] =
    useInputHook("");

  const [isValidForm, setIsValidForm] = useState(false);
  const isEmpty = validator.isEmpty;

  const handleSubmit = async (event) => {
    event.preventDefault();

    setIsValidFName(isEmpty(firstName));
    setIsValidLName(isEmpty(lastName));
    setIsValidEmail(isEmpty(emailAdress));
    setIsValidMobile(isEmpty(mobileNum));
    setIsValidPassword1(isEmpty(password1));
    setIsValidPassword2(isEmpty(password2));

    setIsValidForm(
      isValidFName ||
        isValidLName ||
        isValidMobile ||
        isValidPassword1 ||
        isValidPassword2
    );

    if (!setIsValidForm) {
      return null;
    }

    const formData = {
      firstname: firstName,
      lastname: lastName,
      mobileNo: mobileNum,
      email: emailAdress,
      password: password1,
    };

    const isEmailExistConfig = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email: emailAdress }),
    };

    const registrationConfig = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    };

    const resIsEmailExist = await fetch(
      "https://blooming-basin-66732.herokuapp.com/api/users/email-exists",
      isEmailExistConfig
    );

    const isEmailExist = await resIsEmailExist.json();

    if (isEmailExist) {
      return console.log("Email exist", isEmailExist);
    }

    const resRegistration = await fetch(
      "https://blooming-basin-66732.herokuapp.com/api/users/",
      registrationConfig
    );

    const isRegistrationSuccess = await resRegistration.json();
    console.log(isRegistrationSuccess);
  };

  return (
    <>
      <div className="container">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              className={`form-control ${
                !isValidFName ? "border border-danger" : ""
              } `}
              id="firstName"
              onChange={setFirstName}
              value={firstName}
              name="firstName"
              placeholder="Enter First Name"
            />
          </div>
          <div className="form-group">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              className={`form-control ${
                !isValidLName ? "border border-danger" : ""
              } `}
              id="lastName"
              onChange={setLastName}
              value={lastName}
              name="lastName"
              placeholder="Enter Last Name"
            />
          </div>
          <div className="form-group">
            <label htmlFor="emailAdress">Email address</label>
            <input
              type="email"
              className={`form-control ${
                !isValidEmail ? "border border-danger" : ""
              } `}
              onChange={setEmailAdress}
              id="emailAddress"
              value={emailAdress}
              name="emailAddress"
              placeholder="Enter email"
            />
          </div>
          <div className="form-group">
            <label htmlFor="mobileNum">Mobile Number</label>
            <input
              type="number"
              className={`form-control ${
                !isValidMobile ? "border border-danger" : ""
              } `}
              onChange={setMobileNum}
              id="mobileNum"
              value={mobileNum}
              name="mobileNum"
              placeholder="Enter Mobile Number"
            />
          </div>
          <div className="form-group">
            <label htmlFor="password1">Password</label>
            <input
              type="password"
              className={`form-control ${
                !isValidPassword1 ? "border border-danger" : ""
              } `}
              onChange={setPassword1}
              id="password1"
              value={password1}
              name="password1"
              placeholder="Enter Password"
            />
          </div>
          <div className="form-group">
            <label htmlFor="password2">Re-enter Password</label>
            <input
              type="password"
              className={`form-control ${
                !isValidPassword2 ? "border border-danger" : ""
              } `}
              onChange={setPassword2}
              id="password2"
              value={password2}
              name="password2"
              placeholder="Re-enter Password"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Register
          </button>
        </form>
      </div>
    </>
  );
};

export default Register;
