import React from "react";

import LandingPage from "../../components/LandingPage";
import Products from "../../components/Products";

const Home = () => {
  return (
    <>
      <LandingPage />
     <Products />
    </>
  );
};

export default Home;
