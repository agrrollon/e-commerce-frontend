import React, { useEffect, useState } from "react";
import CartPageForm from "./CartPageForm";
import "./CartPage.css";

const CartPage = () => {
  const [cartProducts, setCartProducts] = useState([]);

  useEffect(() => {
    fetch(`https://blooming-basin-66732.herokuapp.com/api/items/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCartProducts(data);
      });
  }, []);
  console.log(cartProducts);
  const cartDetails = cartProducts.map((cartProduct) => {
    return (
      <CartPageForm
        key={cartProduct._id}
        name={cartProduct.name}
        image={cartProduct.image}
        description={cartProduct.description}
        price={cartProduct.price}
      />
    );
  });

  return <>{cartDetails}</>;
};

export default CartPage;
