import React from "react";
import { Link } from "react-router-dom";
import "./CartPage.css";

const CartPageForm = (props) => {
  return (
    <>
      <div className="small-container cart-page container">
        <table>
          <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th>Subtotal</th>
          </tr>
          <td>
            <div className="cart-info">
              <img src={props.image} alt="..." />
              <div>
                <p>{props.name}</p>
                <small>Price: $ 2000 </small>
                <br />
                <a href="#">Remove</a>
              </div>
            </div>
          </td>
          <td>
            <input type="number" value="1"></input>
          </td>
          <td>$ {props.price} </td>
        </table>
        <div className="total-price">
          <table>
            <tr>
              <td>Subtotal</td>
              <td>$ 2000</td>
            </tr>
            <tr>
              <td>Total</td>
              <td>$ 2000</td>
            </tr>
            <Link to="/checkout" className="btn btn-success">Proceed to Checkout</Link>
          </table>
        </div>
      </div>
    </>
  );
};

export default CartPageForm;
