import { useHistory } from "react-router-dom";
import { useContext, useEffect } from "react";
import AuthContext from "../../store/auth-context";

const Logout = () => {
  const history = useHistory();
  const authCtx = useContext(AuthContext);
  useEffect(() => {
    authCtx.logout();
    history.push("/");
  });
  return <h1>Logging out...</h1>;
};

export default Logout;
