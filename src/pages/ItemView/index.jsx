import React, { useState, useEffect, useContext } from "react";
import ItemView from "./ItemView";
import { useParams } from "react-router-dom";
import AuthContext from "../../store/auth-context";

// const url = window.location.href;

// console.log(url);
// const lastSegment = url.split("/").pop();
// console.log(lastSegment);

const Items = () => {
  const params = useParams();
  const authCtx = useContext(AuthContext);
  console.log(params.id);
  console.log(authCtx.token);
  const [productViews, setProductViews] = useState([]);
  const getItemConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authCtx.token}`,
    },
  };

  useEffect(() => {
    fetch(
      `https://blooming-basin-66732.herokuapp.com/api/items/${params.id}`,
      getItemConfig
    )
  },[])
}
export default Items;
