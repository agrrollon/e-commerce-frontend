import React from "react";
import { Link } from "react-router-dom";

import "./ItemView.css";

const ItemView = (props) => {
    
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-5">
          <div
            id="carouselExampleControls"
            class="carousel slide"
            data-ride="carousel"
          >
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src={props.image} class="d-block w-100" alt="..." />
              </div>
              <div class="carousel-item">
                <img src={props.image} class="d-block w-100" alt="..." />
              </div>
              <div class="carousel-item">
                <img src={props.image} class="d-block w-100" alt="..." />
              </div>
            </div>
            <a
              class="carousel-control-prev"
              href="#carouselExampleControls"
              role="button"
              data-slide="prev"
            >
              <span
                class="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Previous</span>
            </a>
            <a
              class="carousel-control-next"
              href="#carouselExampleControls"
              role="button"
              data-slide="next"
            >
              <span
                class="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div className="col-md-7">
          <p className="newarrival text-center">NEW</p>
          <h2>High Quality Furnitures</h2>
          <p>Product Name: {props.name}</p>
          <p className="price">${props.price}</p>
          <p><b>Availability: In Stock </b></p>
          <p><b>Condition: New</b></p>
          <p><b>Brand: AED Company </b></p>
          <Link to="/addtocart" type="button" className="btn btn-primary cart">Add to Cart</Link>
        </div>
      </div>
    </div>
  );
};

export default ItemView;
