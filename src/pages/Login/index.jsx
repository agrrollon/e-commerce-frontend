import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

import useInputHook from "../../hooks/input/useInputHook";
import AuthContext from "../../store/auth-context";

const Login = () => {
  const history = useHistory();
  const [email, setEmail] = useInputHook("");
  const [password, setPassword] = useInputHook("");

  const authCtx = useContext(AuthContext);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = {
      email: email,
      password: password,
    };

    const loginConfig = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    };

    const resLogin = await fetch(
      "https://blooming-basin-66732.herokuapp.com/api/users/login",
      loginConfig
    );

    const isLoginSuccess = await resLogin.json();

    if (!isLoginSuccess) {
      toast.info("Incorrect username or password", {
        position: "top-center",
      });
      return null;
    }

    authCtx.login(
      isLoginSuccess.access,
      isLoginSuccess.user.firstname,
      isLoginSuccess.user.isAdmin
    );
    console.log(isLoginSuccess);
    history.push("/");
    toast.success(`Welcome Back ${isLoginSuccess.user.firstname}`, {
      position: "top-center",
    });
  };

  return (
    <>
      <div className="container">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label for="emailAdress">Email address</label>
            <input
              type="email"
              className="form-control"
              id="email"
              value={email}
              onChange={setEmail}
              placeholder="Enter email"
            />
          </div>

          <div className="form-group">
            <label for="password1">Password</label>
            <input
              type="password"
              className="form-control"
              id="password1"
              value={password}
              onChange={setPassword}
              placeholder="Enter Password"
            />
          </div>

          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </>
  );
};

export default Login;
