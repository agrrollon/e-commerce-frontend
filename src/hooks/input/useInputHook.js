import { useState } from "react";

const useInputHook = (defaultValue) => {
  const [value, setValue] = useState(defaultValue);
  const [isValid, setIsValid] = useState(true);

  const handleChange = (event) => {
    setValue(event.target.value);
    setIsValid(true);
  };
  const isValidInput = (input) => {
    setIsValid(!input);
  };

  return [value, isValid, handleChange, isValidInput];
};

export default useInputHook;
