import { createContext, useState } from "react";

const AuthContext = createContext({
  token: "",
  name: "",
  isAdmin: "",
  isLoggedIn: false,
  login: (token, name, isAdmin) => {},
  logout: () => {},
});

export const AuthContextProvider = (props) => {
  const initialToken = localStorage.getItem("token");
  const userName = localStorage.getItem("name");
  const userIsAdmin = localStorage.getItem("isAdmin");

  const [token, setToken] = useState(initialToken);
  const [name, setName] = useState(userName);
  const [isAdmin, setUserIsAdmin] = useState(userIsAdmin);

  const userIsLoggedIn = !!token;

  const loginHandler = (token, name, isAdmin) => {
    setToken(token);
    setName(name);
    setUserIsAdmin(isAdmin);
    localStorage.setItem("token", token);
    localStorage.setItem("name", name);
    localStorage.setItem("isAdmin", isAdmin);
  };

  const logoutHandler = () => {
    setToken(null);
    localStorage.removeItem("token");
    localStorage.removeItem("name");
    localStorage.removeItem("isAdmin");
  };

  const contextValue = {
    token: token,
    name: name,
    isAdmin: isAdmin,
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
